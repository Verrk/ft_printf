#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: verrk <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/15 10:38:21 by cpestour          #+#    #+#              #
#    Updated: 2015/04/28 15:00:09 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC=gcc
CFLAGS=-Wall -Werror -Wextra -Iincludes -g
SRC=srcs/printf.c srcs/format.c srcs/flags.c srcs/cformat.c srcs/sformat.c \
srcs/dformat.c srcs/uformat.c srcs/pformat.c srcs/xformat.c srcs/oformat.c
OBJ=$(SRC:.c=.o)
OBJ_LIB=$(addprefix libft/, *.o)
NAME=libftprintf.a

all: lib $(NAME)

lib:
	@make -C libft

$(NAME): $(OBJ)
	@ar rc $@ $^ $(OBJ_LIB)
	@ranlib $@
	@echo "\033[33;32mlibftpintf done\033[33;37m"

%.o: %.c
	@$(CC) -o $@ -c $< $(CFLAGS)

clean:
	@rm -f $(OBJ) *~ srcs/*~ includes/*~
	@echo "\033[33;31mclean done\033[33;37m"

fclean: clean
	@make fclean -C libft
	@rm -f $(NAME)
	@echo "\033[33;31mfclean done\033[33;37m"

re: fclean all
