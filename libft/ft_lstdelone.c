/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/03 19:21:23 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/03 19:21:30 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list **lst, void (*del)(void *, size_t))
{
	if (!lst || !*lst)
		return ;
	if (del)
		del((*lst)->content, (*lst)->content_size);
	else
		free((*lst)->content);
	(*lst)->content = NULL;
	ft_memdel((void **)lst);
}
