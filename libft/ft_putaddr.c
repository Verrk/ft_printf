/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putaddr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/23 17:39:32 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/19 17:29:55 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		init(char *tab, int size)
{
	int		i;

	i = 0;
	while (i < size)
		tab[i++] = '0';
}

void		ft_putaddr(void *ptr)
{
	char	str[12];
	int		i;
	long	num;
	long	num_addr;

	i = 11;
	init(str, 12);
	num_addr = (long)ptr;
	while (i >= 0)
	{
		num = num_addr % 16;
		if (num < 10)
			str[i] = num + '0';
		else
			str[i] = num % 10 + 'A';
		num_addr /= 16;
		i--;
	}
	ft_putstr("0x");
	i = 0;
	while (i < 12)
		ft_putchar(str[i++]);
}
