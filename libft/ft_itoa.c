/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 14:56:01 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/21 19:38:38 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_itoa(int n)
{
	char	*s;
	size_t	i;

	i = ft_nbrlen(n);
	i += (n < 0) ? 1 : 0;
	s = (char *)malloc(i + 1);
	s[i] = '\0';
	if (n < 0)
		s[0] = '-';
	if (n == 0)
		s[0] = '0';
	while (i-- && n)
	{
		s[i] = ft_abs(n % 10) + '0';
		n /= 10;
	}
	return (s);
}

static void	ft_swap_buf(char *buf, int size)
{
	int		i;
	char	tmp;

	i = 0;
	while (i < size)
	{
		tmp = buf[i];
		buf[i] = buf[size];
		buf[size] = tmp;
		i++;
		size--;
	}
}

char		*ft_itoa_base(unsigned int n, int base, int maj)
{
	char	*buf;
	int		i;
	int		tmp;

	buf = (char *)ft_memalloc(17);
	i = 0;
	if (n == 0)
		buf[i++] = '0';
	while (n)
	{
		tmp = n % base;
		if (tmp < 10)
			buf[i] = tmp + '0';
		else
			buf[i] = maj ? tmp + 'A' - 10 : tmp + 'a' - 10;
		i++;
		n /= base;
	}
	ft_swap_buf(buf, --i);
	return (buf);
}

char		*ft_ltoa(unsigned long n, int base)
{
	char	*buf;
	int		i;
	int		tmp;

	buf = (char*)ft_memalloc(17);
	i = 0;
	while (n)
	{
		tmp = n % base;
		buf[i++] = tmp < 10 ? tmp + '0' : tmp + 'a' - 10;
		n /= base;
	}
	ft_swap_buf(buf, --i);
	return (buf);
}
