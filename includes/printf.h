/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 10:41:53 by verrk             #+#    #+#             */
/*   Updated: 2015/12/18 18:02:46 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTF_H
# define PRINTF_H

# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <stdint.h>
# include <wchar.h>
# include "../libft/includes/libft.h"

typedef unsigned long long	t_ull;
typedef unsigned long		t_ul;

typedef struct				s_flags
{
	int						sharp;
	int						zero;
	int						minus;
	int						plus;
	int						space;
	int						width;
	int						prec;
	char					mod;
}							t_flags;

typedef struct s_printf		t_printf;
typedef void				(*t_funptr)(t_printf *);

typedef struct				s_fun
{
	char					c;
	t_funptr				f;
}							t_fun;

struct						s_printf
{
	va_list					va;
	int						ret;
	t_flags					flags;
	t_fun					fun_tab[16];
};

int							ft_printf(const char *format, ...);
void						ft_format(const char *format, t_printf *p);
int							get_flags(const char *format, t_printf *p);
void						c_format(t_printf *p);
void						c_maj_format(t_printf *p);
void						s_format(t_printf *p);
void						s_maj_format(t_printf *p);
void						d_format(t_printf *p);
void						d_maj_format(t_printf *p);
void						u_format(t_printf *p);
void						u_maj_format(t_printf *p);
void						p_format(t_printf *p);
void						x_format(t_printf *p);
void						x_maj_format(t_printf *p);
void						o_format(t_printf *p);
void						o_maj_format(t_printf *p);
void						perc_format(t_printf *p);

#endif
