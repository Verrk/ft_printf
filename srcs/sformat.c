/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sformat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/16 20:26:29 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/01 15:53:34 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void		s_maj_format(t_printf *p)
{
	wchar_t	*s;

	s = ft_wstrdup(va_arg(p->va, wchar_t *));
	if (s == NULL)
	{
		p->ret += ft_putstr("(null)");
		return ;
	}
	if (p->flags.prec >= 0)
		s[p->flags.prec] = (wchar_t)0;
	if (!p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width - ft_wstrlen(s));
	p->ret += ft_putwstr(s, p->flags.prec);
	if (p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width - ft_wstrlen(s));
}

void		s_format(t_printf *p)
{
	char	*s;

	if (p->flags.mod == 'l')
	{
		s_maj_format(p);
		return ;
	}
	s = ft_strdup(va_arg(p->va, char *));
	if (s == NULL)
	{
		p->ret += ft_putstr("(null)");
		return ;
	}
	if (p->flags.prec >= 0 && p->flags.prec <= (int)ft_strlen(s))
		s[p->flags.prec] = '\0';
	if (!p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width - ft_strlen(s));
	p->ret += ft_putstr(s);
	if (p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width - ft_strlen(s));
	free(s);
}
