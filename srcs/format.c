/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 13:19:59 by verrk             #+#    #+#             */
/*   Updated: 2015/12/18 17:55:06 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static void		init_flags(t_flags *flags)
{
	flags->sharp = 0;
	flags->zero = 0;
	flags->minus = 0;
	flags->plus = 0;
	flags->space = 0;
	flags->width = 0;
	flags->prec = -1;
	flags->mod = '\0';
}

static void		no_flags(char c, t_printf *p)
{
	if (!c)
		return ;
	if (!p->flags.minus)
	{
		if (p->flags.zero)
			p->ret += ft_nputchar('0', p->flags.width - 1);
		else
			p->ret += ft_nputchar(' ', p->flags.width - 1);
	}
	p->ret += ft_putchar(c);
	if (p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width - 1);
}

static void		get_perc(const char **format, t_printf *p)
{
	int			i;

	init_flags(&(p->flags));
	(*format)++;
	(*format) += get_flags(*format, p);
	i = 0;
	while (p->fun_tab[i].c)
	{
		if (p->fun_tab[i].c == **format)
		{
			p->fun_tab[i].f(p);
			break ;
		}
		i++;
	}
	if (i == 15)
		no_flags(**format, p);
}

void			perc_format(t_printf *p)
{
	if (!p->flags.minus)
	{
		if (p->flags.zero)
			p->ret += ft_nputchar('0', p->flags.width - 1);
		else
			p->ret += ft_nputchar(' ', p->flags.width - 1);
	}
	p->ret += ft_putchar('%');
	if (p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width - 1);
}

void			ft_format(const char *format, t_printf *p)
{
	while (*format)
	{
		if (*format == '%')
			get_perc(&format, p);
		else
			p->ret += ft_putchar(*format);
		format++;
	}
}
