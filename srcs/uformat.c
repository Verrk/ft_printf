/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   uformat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/17 17:35:20 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/29 17:28:36 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static void		print_sign(t_printf *p)
{
	if (p->flags.plus)
		p->ret += ft_putchar('+');
	else if (p->flags.space)
		p->ret += ft_putchar(' ');
}

static void		print_space(t_printf *p, t_ull u, int start)
{
	int			l;

	if (p->flags.plus || p->flags.space)
		l = 1;
	else
		l = 0;
	l += p->flags.prec > (int)ft_nbrlen_u(u) ?
		p->flags.prec : (int)ft_nbrlen_u(u);
	if (start)
	{
		if (p->flags.zero && !p->flags.minus && p->flags.prec == -1)
		{
			print_sign(p);
			p->ret += ft_nputchar('0', p->flags.width - l);
		}
		else if (!p->flags.minus && p->flags.width > p->flags.prec)
		{
			p->ret += ft_nputchar(' ', p->flags.width - l);
			print_sign(p);
		}
		else
			print_sign(p);
	}
	if (p->flags.minus && !start)
		p->ret += ft_nputchar(' ', p->flags.width - l);
}

static void		print_u(t_printf *p, t_ull u)
{
	print_space(p, u, 1);
	p->ret += ft_nputchar('0', p->flags.prec - ft_nbrlen_u(u));
	if (u != 0 || (u == 0 && p->flags.prec != 0))
		p->ret += ft_putnbr_u(u);
	else if (u == 0 && p->flags.width)
		p->ret += ft_putchar(' ');
	print_space(p, u, 0);
}

void			u_format(t_printf *p)
{
	t_ull		u;

	if (p->flags.mod == 'h')
		u = (unsigned short)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'H')
		u = (unsigned char)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'l')
		u = va_arg(p->va, unsigned long);
	else if (p->flags.mod == 'L')
		u = va_arg(p->va, unsigned long long);
	else if (p->flags.mod == 'j')
		u = va_arg(p->va, uintmax_t);
	else if (p->flags.mod == 'z')
		u = va_arg(p->va, size_t);
	else
		u = va_arg(p->va, unsigned int);
	print_u(p, u);
}

void			u_maj_format(t_printf *p)
{
	t_ul		u;

	u = va_arg(p->va, unsigned long);
	print_u(p, u);
}
