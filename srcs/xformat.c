/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xformat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/21 19:50:00 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/29 17:28:55 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static void		print_sign(t_printf *p, char *str, int maj)
{
	if (p->flags.sharp && str[0] != '0')
	{
		p->ret += ft_putchar('0');
		if (maj)
			p->ret += ft_putchar('X');
		else
			p->ret += ft_putchar('x');
		p->flags.width -= 2;
	}
}

static void		print_space(t_printf *p, char *str, int start, int maj)
{
	int			l;

	l = p->flags.sharp && str[0] != '0' ? 2 : 0;
	l += p->flags.prec > (int)ft_strlen(str) ? p->flags.prec :
		(int)ft_strlen(str);
	if (start)
	{
		if (p->flags.zero && !p->flags.minus && p->flags.prec == -1)
		{
			print_sign(p, str, maj);
			p->ret += ft_nputchar('0', p->flags.width - l);
		}
		else if (!p->flags.minus && p->flags.width > p->flags.prec)
		{
			p->ret += ft_nputchar(' ', p->flags.width - l);
			print_sign(p, str, maj);
		}
		else
			print_sign(p, str, maj);
	}
	if (p->flags.minus && !start)
		p->ret += ft_nputchar(' ', p->flags.width - l);
}

static void		print_x(t_printf *p, t_ull x, int maj)
{
	char		*str;

	str = ft_itoa_base(x, 16, maj);
	print_space(p, str, 1, maj);
	p->ret += ft_nputchar('0', p->flags.prec - ft_strlen(str));
	if (x != 0 || (x == 0 && p->flags.prec != 0))
		p->ret += ft_putstr(str);
	else if (x == 0 && p->flags.width)
		p->ret += ft_putchar(' ');
	print_space(p, str, 0, maj);
	free(str);
}

void			x_format(t_printf *p)
{
	t_ull		x;

	if (p->flags.mod == 'h')
		x = (unsigned short)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'H')
		x = (unsigned char)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'l')
		x = va_arg(p->va, unsigned long);
	else if (p->flags.mod == 'L')
		x = va_arg(p->va, unsigned long long);
	else if (p->flags.mod == 'j')
		x = va_arg(p->va, uintmax_t);
	else if (p->flags.mod == 'z')
		x = va_arg(p->va, size_t);
	else
		x = va_arg(p->va, unsigned int);
	print_x(p, x, 0);
}

void			x_maj_format(t_printf *p)
{
	t_ull		x;

	if (p->flags.mod == 'h')
		x = (unsigned short)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'H')
		x = (unsigned char)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'l')
		x = va_arg(p->va, unsigned long);
	else if (p->flags.mod == 'L')
		x = va_arg(p->va, unsigned long long);
	else if (p->flags.mod == 'j')
		x = va_arg(p->va, uintmax_t);
	else if (p->flags.mod == 'z')
		x = va_arg(p->va, size_t);
	else
		x = va_arg(p->va, unsigned int);
	print_x(p, x, 1);
}
